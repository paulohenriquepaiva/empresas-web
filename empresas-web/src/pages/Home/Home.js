import React, { Component } from 'react'
import logo from '../../assets/img/white-logo.png'
import search from '../../assets/icon/search.png'
import './Home.css'

class Home extends Component {
    render(){
        return(
            <div className="content" fluid style={{ height: "100vh", backgroundColor: '#eeecdb' }}>
                <div className="navbar">
                    <img src={logo} className="logo rounded mx-auto d-block" alt="logo" />
                    <img src={search} className="search rounded float-right" alt="search" />
                </div>
                <div className="content-inicial">
                    <p className="text-center" style={{ fontSize: '2rem' }}>Clique na busca para iniciar</p>
                </div>
            </div>
        )
    }
}

export default Home;