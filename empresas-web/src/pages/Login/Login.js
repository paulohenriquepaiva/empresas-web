import React, { useState} from 'react'
import logo from '../../assets/img/logo.png'
import email from '../../assets/icon/email.png'
import password from '../../assets/icon/password.png'
import error from '../../assets/icon/error.png'
import './Login.css';
import API from '../../service/api'
import { useHistory } from "react-router-dom";

function Login() {

  let history = useHistory();

  async function handlerSubmit() {
    await API.post('/users/auth/sign_in', { email: "testeapple@ioasys.com.br", password: "12341234" })
      .then(result => {
        if (result.data.success) {
          localStorage.setItem("credencials", JSON.stringify({ token: result.headers['access-token'], uid: result.headers.uid, client: result.headers.client }))
          history.push('/home')
        }
      })
      .catch(error => console.log(error))
  }  

  return (
    <div className="container-fluid" fluid style={{ height: "100vh", backgroundColor: '#eeecdb' }}>
      <div className="box">
        <img src={logo} className="image rounded mx-auto d-block" alt="logo" />
        <h3 className="text-center title">Bem vindo ao<br />Empresas</h3>
        <p className="text-center subtitle">Lorem ipsum dolor sit amet, contetur<br />adipiscing elit. Nunc accumsan.</p>
        <form style={{ marginTop: '2.969rem' }}>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <img src={email} className="img-responsive icon" alt="email" />
            </div>
            <input type="email" className="form-control custom-input" id="exampleInputPassword1" placeholder="E-mail" />
            <div className="input-group-prepend">
              <img src={error} className="img-responsive icon-error" alt="error" />
            </div>
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <img src={password} className="img-responsive icon" alt="password" />
            </div>
            <input type="password" className="form-control custom-input" id="exampleInputPassword1" placeholder="Senha" />
            <div className="input-group-prepend">
              <img src={error} className="img-responsive icon-error" alt="error" />
            </div>
          </div>
          <p className="text-center" style={{ color: "#ff0f44" }}>Credenciais informadas são inválidas, tente novamente.</p>
          <div className="input-group text-center mb-3">
            <button onClick={() => handlerSubmit()} type="button" class="btn">ENTRAR</button>
          </div>
        </form>
      </div>
    </div>
  )

}

export default Login;
